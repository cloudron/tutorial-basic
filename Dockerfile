FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ADD server.js /app/code/server.js

CMD [ "node", "/app/code/server.js" ]

